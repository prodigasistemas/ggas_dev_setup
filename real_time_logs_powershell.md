# Procedimentos para visualizar logs em tempo real no windows. 

A função tem como objetivo facilitar o monitoramento de logs em tempo real. Ele realiza as seguintes ações:

1. Lista os arquivos de log disponíveis: Mostra todos os arquivos na pasta padrão de logs, no caso: do Tomcat (C:\Program Files\Apache Software Foundation\Tomcat 8.0\logs).

2. Solicita o nome de um arquivo de log: Permite que o usuário escolha qual arquivo deseja monitorar.

3. Solicita o número das últimas linhas para exibir: O usuário informa quantas linhas mais recentes do arquivo devem ser exibidas.

4. Monitora o arquivo em tempo real: Exibe as últimas linhas especificadas do arquivo e continua monitorando para mostrar novas adições ao log (similar ao comando tail -nf do Linux, onde n é o número de linhas recentes a serem exibidas.)

Esse script é útil para desenvolvedores e administradores que precisam acompanhar logs para depuração, análise de erros ou monitoramento do comportamento do servidor.

#

- Em um terminal PowerShell executar os seguites passos: 
#
- Localizar ou criar o arquivo de perfil

        $PROFILE
#
- Para verificar o caminho completo, execute:

        Write-Host $PROFILE
#
- Se o arquivo não existir, você pode criá-lo com o seguinte comando:

        if (-not (Test-Path -Path $PROFILE)) {
            New-Item -ItemType File -Path $PROFILE -Force
        }
#
- Editar o arquivo de perfil

        notepad $PROFILE
#
- Adicionar a função ao perfil

    function logs {
        # Definir o caminho para a pasta de logs
        $logPath = "C:\Program Files\Apache Software Foundation\Tomcat 8.0\logs"

        # Listar arquivos na pasta de logs
        Write-Host "Listando arquivos na pasta de logs: $logPath" -ForegroundColor Cyan
        Get-ChildItem -Path $logPath | ForEach-Object { $_.Name }

        # Solicitar o nome do arquivo ao usuário
        Write-Host "  "
        $RECEBER_NOME_DO_ARQUIVO = Read-Host "Digite o nome do arquivo para monitorar (ex: catalina.out)"

        # Verificar se o arquivo existe
        $filePath = Join-Path -Path $logPath -ChildPath $RECEBER_NOME_DO_ARQUIVO
        if (!(Test-Path -Path $filePath)) {
            Write-Host "O arquivo '$RECEBER_NOME_DO_ARQUIVO' não foi encontrado." -ForegroundColor Red
            return
        }

        # Solicitar o número de linhas ao usuário com valor padrão de 5
        Write-Host "  "
        $RECEBER_NUMERO_LINHAS = Read-Host "Digite o número de linhas para exibir (padrão: 5)"
        if ([string]::IsNullOrWhiteSpace($RECEBER_NUMERO_LINHAS)) {
            $RECEBER_NUMERO_LINHAS = 5
        } elseif (-not ($RECEBER_NUMERO_LINHAS -as [int])) {
            Write-Host "Entrada inválida. Por favor, insira um número válido." -ForegroundColor Yellow
            return
        }

        # Executar o comando Get-Content com os parâmetros fornecidos
        Write-Host "Monitorando o arquivo '$RECEBER_NOME_DO_ARQUIVO' com as últimas $RECEBER_NUMERO_LINHAS linhas..." -ForegroundColor Green
        try {
            Get-Content -Path $filePath -Tail $RECEBER_NUMERO_LINHAS -Wait
        } catch {
            Write-Host "Erro ao tentar monitorar o arquivo: $_" -ForegroundColor Red
        }
    }
    
#
- Ajustar o path dos logs, se necessário.
#
- Salvar o arquivo de perfil
#
- Carregar o perfil

        . $PROFILE
#
- Usar a função

        logs
