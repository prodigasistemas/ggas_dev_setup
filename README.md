# [GGAS DEV Setup](https://gitlab.com/prodigasistemas/ggas_dev_setup) - [Prodiga Sistemas](https://www.prodigasistemas.com.br/)

Esse projeto instala a infraestrutura de desenvolvimento do GGAS através de containers, banco Oracle e aplicação Tomcat, automatizando o processo de configuração e deploy do projeto.

## Configuração de Branch

    - GGAS:
        - BahiaGas: bahiagas
     
        - Algas: algas


## Pré-requisitos

- Ubuntu 18.04/20.04/+


- Espaço livre ~ 30GB


- [Docker](https://docs.docker.com/engine/install/)


- [JDK 1.8](https://www.oracle.com/br/java/technologies/javase/javase8u211-later-archive-downloads.html)
    - faça o download do pacote java, descompacte e mova a pasta para o destino final, ex: /opt

<!-- 
- [Oracle Docker Images](https://github.com/oracle/docker-images)

-       git clone https://github.com/oracle/docker-images.git
-       ./docker-images/OracleDatabase/SingleInstance/dockerfiles/buildContainerImage.sh -xp -v 21.3.0
-       docker image tag oracle/database:21.3.0-xe bahiagas/oracle:21.3.0-xe 
-->


- [NodeJS - 10.24.1 / NPM - 6.14.12](https://tecadmin.net/how-to-install-nvm-on-ubuntu-20-04/#:~:text=1%20Prerequisites.%20You%20must%20have%20a%20running%20Ubuntu,versions%20using%20nvm.%20And%20use%20the...%20More%20)

        nvm list
        nvm install 10.24.1
        nvm use 10.24.1
        nvm uninstall 10.24.1
            nvm alias default 10.24.1
        node -v
        npm -v

<!-- 

- [NPM - 6.14.12](https://www.2daygeek.com/install-nodejs-on-ubuntu-centos-debian-fedora-mint-rhel-opensuse/)

-->

- Linux $PATH

    - Edite o arquivo `~/.bash_aliases` ou `~/.bashrc` e acrescente as variáveis referentes ao PATH.

            vi ~/.bash_aliases    ou    vi ~/.bashrc

            # JAVA
            #export JAVA_HOME=/path/to/java
            export JAVA_HOME=/opt/jdk1.8.0_431
            export PATH=$PATH:$JAVA_HOME/bin

            # NPM
            export NVM_DIR="$HOME/.nvm"
            [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
            [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

